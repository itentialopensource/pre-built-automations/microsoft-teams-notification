# Microsoft - Teams - REST

## Overview

Project for use cases around Microsoft Teams using REST protocol


## Workflows


<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Overview</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href='https://gitlab.com/itentialopensource/pre-built-automations/microsoft-teams-rest/-/blob/master/documentation/Send Notification - Microsoft - Teams - REST.md' target='_blank'>Send Notification - Microsoft - Teams - REST</a></td>
      <td>Send notification on Microsoft teams using webhook key</td>
    </tr>
  </tbody>
</table>


## External Dependencies

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Microsoft Teams</td>
      <td></td>
      <td>v1.0</td>
    </tr>
  </tbody>
</table>

## Adapters

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Version</th>
      <th>Configuration Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://gitlab.com/itentialopensource/adapters/notification-messaging/adapter-msteams">adapter-msteams</a></td>
      <td>^0.6.0</td>
      <td></td>
    </tr>
  </tbody>
</table>
# Send Notification - Microsoft - Teams - REST

## Table of Contents

- [Send Notification - Microsoft - Teams - REST](#send-notification---microsoft---teams---rest)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Getting Started](#getting-started)
    - [Supported IAP Versions](#supported-iap-versions)
    - [External Dependencies](#external-dependencies)
    - [Adapters](#adapters)
    - [How to Install](#how-to-install)
    - [Testing](#testing)
  - [Using this Workflow Project](#using-this-workflow-project)
    - [Entry Point IAP Component](#entry-point-iap-component)
    - [Inputs](#inputs)
    - [Outputs](#outputs)
    - [Query Output](#query-output)
    - [Example Inputs and Outputs](#example-inputs-and-outputs)
    - [API Links](#api-links)
  - [Support](#support)

## Overview

Send notification on Microsoft teams using webhook key

Capabilities include:
- Send notification on Microsoft Teams using webhook key





## Getting Started

### Supported IAP Versions

Itential Workflow Projects are built and tested on particular versions of IAP. In addition, Workflow Projects are often dependent on external systems and as such, these Workflow Projects will have dependencies on these other systems. This version of **Send Notification - Microsoft - Teams - REST** has been tested with:


- IAP **2023.2**



### External Dependencies

This version of **Send Notification - Microsoft - Teams - REST** has been tested with:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>OS Version</th>
      <th>API Version</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Microsoft Teams</td>
      <td></td>
      <td>v1.0</td>
    </tr>
  </tbody>
</table>




### Adapters

This version of **Send Notification - Microsoft - Teams - REST** has been tested with:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Version</th>
      <th>Configuration Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="https://gitlab.com/itentialopensource/adapters/notification-messaging/adapter-msteams">adapter-msteams</a></td>
      <td>^0.6.0</td>
      <td></td>
    </tr>
  </tbody>
</table>



### How to Install

To install the Workflow Project:

- Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Supported IAP Versions](#supported-iap-versions) section in order to install the Workflow Project.
- Import the Workflow Project in [Admin Essentials](https://docs.itential.com/docs/importing-a-prebuilt-4).

### Testing

While Itential tests this Workflow Project and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this Workflow Project into a development/testing environment in which you can test the Workflow Project.

## Using this Workflow Project


### Entry Point IAP Component

The primary IAP component to run **Send Notification - Microsoft - Teams - REST** is listed below:

<table>
  <thead>
    <tr>
      <th>IAP Component Name</th>
      <th>IAP Component Type</th>
    </tr>
  </thead>
  <tbody>
      <td>Send Notification - Microsoft - Teams - REST</td>
      <td>Workflow</td>
    </tr>
  </tbody>
</table>

### Inputs

The following table lists the inputs for **Send Notification - Microsoft - Teams - REST**:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
      <th>Example Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>text</td>
      <td>string</td>
      <td>yes</td>
      <td>Notification text</td>
      <td><pre lang="json">Are you available for a call?</pre></td>
    </tr>    <tr>
      <td>title</td>
      <td>string</td>
      <td>yes</td>
      <td>Notification title</td>
      <td><pre lang="json">Sync up</pre></td>
    </tr>    <tr>
      <td>webhookKey</td>
      <td>string</td>
      <td>yes</td>
      <td>MS teams webhook key</td>
      <td><pre lang="json">webhookb2/16e71d2b-2b61-40d8-a163-a1c39aaff787@a8413c80-2495-4b90-a922-6e9ff2974531/IncomingWebhook/dd4f6837b7f84a38805428cd54bf81f4/cfacf732-5cfc2</pre></td>
    </tr>    <tr>
      <td>adapterId</td>
      <td>string</td>
      <td>yes</td>
      <td>Microsoft adapter instance name configured in IAP</td>
      <td><pre lang="json">MS Teams</pre></td>
    </tr>
  </tbody>
</table>



### Outputs

The following table lists the outputs for **Send Notification - Microsoft - Teams - REST**:

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Type</th>
      <th>Description</th>
      <th>Example Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>result</td>
      <td>object</td>
      <td>Result has a key response which if equal to 1 means notification was sent successfully</td>
      <td><pre lang="json">1</pre></td>
    </tr>
  </tbody>
</table>



### Query Output


  

The following items show how to query successful results from the output of **Send Notification - Microsoft - Teams - REST**:

    
#### response

`result.response`

    
  
  
The following items show how to query failure results from the output of **Send Notification - Microsoft - Teams - REST**:

    
#### errorMessage

`errorMessage.IAPerror.raw_response.response`

    
  




### Example Inputs and Outputs

  
#### Example 1

    
Input:
<pre>{
	"text": "World",
	"title": "Hello",
	"webhookKey":"webhookb2/16e71d2b-2b61-40d8-a163-a1c39aaff787@a8413c80-2495-4b90-a922-6e9ff2974531/IncomingWebhook/dd4f6837b7f84a38805428cd54bf81f4/cfacf732-fd60-4702-8026-8689c9b5cfc2",
	"adapterId": "MS Teams"
} </pre>

    
    
Output:
<pre>{
  "icode": "AD.200",
  "response": 1,
  "headers": {
    "content-length": "1",
    "content-type": "text/plain; charset=utf-8",
    "server": "Microsoft-IIS/10.0",
    "request-id": "2af9267d-63c1-d09d-4439-e06d066cfe97",
    "strict-transport-security": "max-age=31536000; includeSubDomains; preload",
    "alt-svc": "h3=\":443\",h3-29=\":443\"",
    "x-preferredroutingkeydiagnostics": "1",
    "x-calculatedfetarget": "CYXPR03CU003.internal.outlook.com",
    "x-backendhttpstatus": "200, 200",
    "x-calculatedbetarget": "CY4PR1801MB1864.NAMPRD18.PROD.OUTLOOK.COM",
    "x-end2endlatencyms": "2460",
    "x-rum-validated": "1",
    "x-rum-notupdatequeriedpath": "1",
    "x-rum-notupdatequerieddbcopy": "1",
    "x-proxy-routingcorrectness": "1",
    "x-proxy-backendserverstatus": "200",
    "x-bepartition": "CLNAMPRD18CYS01",
    "x-feproxyinfo": "BLAPR03CA0169.NAMPRD03.PROD.OUTLOOK.COM",
    "x-feefzinfo": "MNZ",
    "ms-cv": "fSb5KsFjndBEOeBtBmz+lw.1.1",
    "x-feserver": "CYXPR03CA0065, BLAPR03CA0169",
    "x-firsthopcafeefz": "MNZ",
    "x-powered-by": "ASP.NET",
    "date": "Fri, 04 Aug 2023 15:45:54 GMT",
    "connection": "close"
  },
  "metrics": {
    "code": 200,
    "timeouts": 0,
    "redirects": 0,
    "retries": 0,
    "tripTime": 2578,
    "isThrottling": false,
    "capabilityTime": "2583ms"
  }
} </pre>

    
  


### API Links


No API Links provided.


## Support

Please use your Itential Customer Success account if you need support when using **Send Notification - Microsoft - Teams - REST**.